#!/bin/bash

echo "Crawling file system!!"
./crawl.sh stage1 $1
echo "Adding basic metadata !!"
./annotate.sh stage1.xml stage2.xml
echo "Adding CSV metadata !!"
./csv_annotate.sh stage2.xml stage3.xml
echo "Aggreating metadata !!"
./summarize.sh stage3.xml $2
 

