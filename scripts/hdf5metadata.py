#!/usr/bin/python 

import h5py
import sys

def visitor_func(name, node):
    if isinstance(node, h5py.Dataset):
		print "<Dataset name=\""+name+ "\">"
		print "</Dataset>"
    elif isinstance(node, h5py.Group):
        	print "<Group name=\""+name+"\" >"
   		node.visititems(visitor_func)
		print "</Group>"
    elif isinstance(node, h5py.Attr):
	    	print "<Attr name=\""+name+"\" >"
		node.visititems(visitor_func)
		for key, val in node.attrs.iteritems():
		        print "    %s: %s" % (key, val)
		print "</Attr>"
    elif isinstance(node, h5py.Dim):
		print "<Dim name=\""+name+"\" >"
		print "</Dim>"	

print "<metadata>"
with h5py.File(sys.argv[1], 'r') as f:
    f.visititems(visitor_func)
print "</metadata>"
import h5py
