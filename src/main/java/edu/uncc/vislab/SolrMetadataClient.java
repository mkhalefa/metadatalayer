package edu.uncc.vislab;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.*;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SolrMetadataClient {
	private Log log = LogFactory.getLog(SolrMetadataClient.class);

	static public String SOLR_URL = "http://localhost:8983/solr/z";
	SolrClient solr = new HttpSolrClient(Conf.getString("solr.host", SOLR_URL));

	public static SolrInputDocument AddMetadata(String json, String uuids[]) {
		SolrInputDocument doc = new SolrInputDocument();

		ArrayList<String> keywordsArray = new ArrayList<>();
		ArrayList<String> valuesArray = new ArrayList<>();
		getKeyVal(json, doc, keywordsArray, valuesArray);
		doc.addField("K", keywordsArray);
		doc.addField("V", valuesArray);

		return doc;
	}

	public static SolrInputDocument AddMetadata(String json) {
		SolrInputDocument doc = new SolrInputDocument();

		ArrayList<String> keywordsArray = new ArrayList<>();
		ArrayList<String> valuesArray = new ArrayList<>();
		getKeyVal(json, doc, keywordsArray, valuesArray);
		doc.addField("K", keywordsArray);
		doc.addField("V", valuesArray);

		return doc;
	}
	
	public static void getKeyVal(String json, SolrInputDocument doc, ArrayList<String> keys, ArrayList<String> values) {
		JsonReader jsonReader = Json.createReader(new StringReader(json));
		JsonArray arr = jsonReader.readArray();
		for (JsonObject vv : arr.getValuesAs(JsonObject.class)) {

			Iterator it = vv.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry) it.next();

				String key = (String) pair.getKey();
				// key=KeywordMapping.get(key);
				JsonValue val = (JsonValue) pair.getValue();
				String v = val.toString().replaceAll("^\"|\"$", "");

				if (SolrSchema.inSchema(key)) {
					doc.addField(key, v);
				} else {
					keys.add(key);
					values.add(v);
				}
			}
		}
		jsonReader.close();
	}

	public void add(SolrInputDocument doc) throws SolrServerException, IOException {
		solr.add(doc);
		solr.commit();
	}

	public SolrDocumentList search(String k) {
		try {
			SolrQuery query = new SolrQuery();
			query.set("q", k);
			QueryResponse response = solr.query(query);
			SolrDocumentList list = response.getResults();
			return list;
		} catch (Exception e) {
			return null;
		}
	}

	public void updates(SolrInputDocument doc) throws SolrServerException, IOException {

		doc.addField("id", "b-10");
		Map<String, String> cmd1 = new HashMap<String, String>();
		Map<String, String> cmd2 = new HashMap<String, String>();
		cmd1.put("set", "newvalue");
		cmd2.put("add", "additionalvalue");
		doc.addField("cat", cmd1);
		doc.addField("field0", cmd2, 0);

	}

	@Override
	protected void finalize() throws Throwable {
		solr.close();
	}

}

/*
 * static void getColumnType(String json, SolrInputDocument doc,
 * ArrayList<String> keys, ArrayList<String> values) { JsonReader jsonReader =
 * Json.createReader(new StringReader(json)); JsonArray arr =
 * jsonReader.readArray(); for (JsonObject vv :
 * arr.getValuesAs(JsonObject.class)) {
 * 
 * Iterator it = vv.entrySet().iterator(); while (it.hasNext()) { Map.Entry pair
 * = (Map.Entry) it.next();
 * 
 * String key = (String) pair.getKey(); JsonValue val = (JsonValue)
 * pair.getValue(); String v = val.toString().replaceAll("^\"|\"$", "");
 * 
 * if (generickey.contains(key)) { doc.addField(key, v); } else { keys.add(key);
 * values.add(v); } } } jsonReader.close(); }
 */

/*
 * public static SolrInputDocument Addcols(String json, String uuids[]) {
 * SolrInputDocument doc = new SolrInputDocument(); for (int i = 1; i <
 * ids.length; i++) { doc.addField(ids[i], uuids[i]); } ids[0] =
 * UUID.randomUUID().toString(); doc.addField("type", "cols"); ArrayList<String>
 * cols = new ArrayList<>(); ArrayList<String> types = new ArrayList<>();
 * getKeyVal(json, doc, cols, types); doc.addField("K", cols); doc.addField("V",
 * types);
 * 
 * return doc; }
 */

// for (int i = 0; i < ids.length; i++) {
// doc.addField(ids[i], uuids[i]);
// }
// doc.addField("type", "file");