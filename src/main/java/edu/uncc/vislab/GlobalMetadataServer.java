package edu.uncc.vislab;

import java.io.*;
import java.util.*;

import static spark.Spark.*;

public class GlobalMetadataServer extends MetadataServer {

	static void id() {

		get("/id", (request, response) -> {

		return "Global Metadata Server";
			
		});
	}

	public static void main(String[] args) {
		int portip = Conf.getInt("globalserver.port");
		dir=Conf.getString("metadata.dir");
		System.out.println(portip);
		port(portip);
		metadata();
		metadata_form();
		dataset();
		search();
		list();
		
		id();
	}
}
