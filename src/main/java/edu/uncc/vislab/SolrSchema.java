package edu.uncc.vislab;

import java.util.HashSet;

public class SolrSchema {
	/*
	 * static String[] ids = new String[3]; static { ids[0] = "id"; ids[1] =
	 * "parentid"; ids[2] = "datasetid"; }
	 */
	static HashSet<String> schema = new HashSet<String>();
	static {
		schema.add("id"); 
		schema.add("datasetid"); 
		schema.add("name"); 
		schema.add("domain");
		schema.add("type"); 
		schema.add("subtype");  
		schema.add("columns");  
		schema.add("keywords");  
		schema.add("lastupdated");  
		schema.add("size"); 
		schema.add("pac");  
		schema.add("versionid");  
		schema.add("current");  
		schema.add("statistics");  
		schema.add("link");  
		schema.add("notes");
		schema.add("org");		 

	}
	
	static HashSet<String> ignore = new HashSet<String>();
	static {
		ignore.add("pac");
		ignore.add("_version_");
		ignore.add("version");
		ignore.add("versionid");
		ignore.add("iscurrent");
	}
	
	public static HashSet<String> getSchema()
	{
	return schema;
	}
	
	
	public static boolean inSchema(String key) {
		return (schema.contains(key));
	}
	
	public static boolean inIgnored(String key) {
		return (ignore.contains(key));
	}

}
