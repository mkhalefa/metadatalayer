package edu.uncc.vislab;

import org.apache.commons.configuration2.builder.fluent.*;
import org.apache.commons.configuration2.builder.*;
import org.apache.commons.configuration2.*;
import org.apache.commons.configuration2.ex.*;

public class Conf {
	static Configuration config;

	static void load() {
		Parameters params = new Parameters();
		FileBasedConfigurationBuilder<FileBasedConfiguration> builder = new FileBasedConfigurationBuilder<FileBasedConfiguration>(
				PropertiesConfiguration.class).configure(params.properties().setFileName("metadata.properties"));
		try {
			config = builder.getConfiguration();

		} catch (ConfigurationException cex) {
			// loading of the configuration file failed
		}
	}

	public static int getInt(String port) {
		if (config == null)
			load();
		return config.getInt(port);
	}

	public static String getString(String s, String d) {
		if (config == null)
			load();
		return config.getString(s, d);
	}

	public static String getString(String s) {
		if (config == null)
			load();
		return config.getString(s);
	}
}
