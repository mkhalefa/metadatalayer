package edu.uncc.vislab;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Spliterators;
import java.util.Vector;

import javax.xml.bind.DatatypeConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrDocument;

public class Search {
	private Log log = LogFactory.getLog(Search.class);

	static class Op {
		public Op() {
		}

		boolean Compare(String val) {
			boolean numbers = true;
			double n = 0;
			double v = 0;
			try {
				n = Double.parseDouble(op2);
				v = Double.parseDouble(val);
			} catch (Exception e) {
				numbers = false;
			}
			if (numbers) {

				if (op.equals("="))
					return n == v;
				else if (op.equals("<"))
					return v > n;
				else if (op.equals(">"))
					return v < n;

			} else {
				if (op.equals("="))
					return val.equals(op2);
				else if (op.equals("<"))
					return val.compareTo(op2) == -1;
				else if (op.equals(">"))
					return val.compareTo(op2) == 1;

			}

			return false;
		}

		public String op1;
		public String op2;
		public String op;

		@Override
		public String toString() {
			return toString(op1);
		}

		public String toString(String x) {
			if (op.equals("="))
				return x + ":" + op2;
			else if (op.equals("<"))
				return x + ":" + "[* TO " + op2 + "]";
			else if (op.equals(">"))
				return x + ":" + "[" + op2 + " TO *]";
			return x + op2;
		}
	}

	public static HashMap<String, Op> operations = new HashMap<>();

	static boolean op(String str, Op o) {
		if (str.contains("=")) {
			String[] strs = str.split("=");
			o.op1 = strs[0];
			o.op2 = strs[1];
			o.op = "=";
			return true;
		}
		if (str.contains(">")) {
			String[] strs = str.split(">");
			o.op1 = strs[0];
			o.op2 = strs[1];
			o.op = ">";
			return true;
		}
		if (str.contains("<")) {
			String[] strs = str.split("<");
			o.op1 = strs[0];
			o.op2 = strs[1];
			o.op = "<";
			return true;
		}
		return false;
	}

	static String computeSolrQuery(String[] args) {
		String q = "";
		for (String arg : args) {
			if (arg.trim().isEmpty())
				continue;
			Op o = new Op();

			if (op(arg, o)) {

				if (SolrSchema.inSchema(o.op1)) {
					q = q + o.toString() + " AND ";

				} else {
					q = q + " _text_: " + o.op1 + " AND " + o.toString("_text_") + " AND ";
					operations.put(o.op1, o);
				}
			} else {

				if (SolrSchema.inSchema(arg)) {

				} else {
					q = q + " _text_: " + arg + " AND ";
				}
			}
		}
		String result = q;
		if (q.contains(" AND ")) {
			result = q.substring(0, q.lastIndexOf(" AND "));
			// log.warn (result);
		}
		return result;
	}

	static public String ahref(SolrDocument doc) {

		Object o = doc.getFieldValue("pac");
		String data = "";

		if (o instanceof ArrayList<?>) {
			if (((ArrayList<?>) o).get(0) instanceof String) {
				ArrayList<String> pacs = (ArrayList<String>) o;
				data = pacs.get(0);
			}
		}
		if (o instanceof String) {
			data = (String) o;
		}
		
		Object oo=doc.getFieldValue("pac_file");
		String filename="a.json";
		
		if (oo instanceof ArrayList<?>) {
			if (((ArrayList<?>) oo).get(0) instanceof String) {
				ArrayList<String> g = (ArrayList<String>) oo;
				filename = g.get(0);
			}
		}
		if (oo instanceof String) {
			filename = (String) oo;
		}
		
		String encodeddata = "data:text/general;base64," + DatatypeConverter.printBase64Binary(data.getBytes());
		
		String l = "<a href=\"" + encodeddata + "\" download=\""+ filename+ "\">";
		l = l + "Configuration file" + "</a>";
		return l;
	}

	static public String ahref_(SolrDocument doc) {

		String id = "a" + doc.getFieldValue("id");
		String data = (String) doc.getFieldValue("pac");
		String l = "<javascript>";
		l = l + " var " + id + " = window.document.createElement('a'); \n";
		l = l + id + ".href = window.URL.createObjectURL(new Blob(['" + data + "'], {type: 'text/general'})); \n";
		l = l + id + ".download = 'script' \n";

		l = l + "document.body.appendChild(" + id + ") \n";
		l = l + id + ".click(); \n";

		// Remove anchor from body
		l = l + "document.body.removeChild(" + id + "); \n";
		return l;
	}

	static public String toHtml(SolrDocument doc) {
		String l = "";
		Iterator it = doc.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			String k = (String) pair.getKey();
			String v = " "+ pair.getValue() +" ";
			if (SolrSchema.inIgnored(k))
				continue;
			if (v.trim().length() == 0)
				continue;
			l = l + "<b>" + k + ":</b> " + v + "</br>";
		}
		l = l + ahref(doc) + "</br>";
		return l;
	}

	static public List<String> run(String[] args) {
		List<String> l = new Vector<String>();
		if (args.length > 0) {
			String query = computeSolrQuery(args);
			// System.out.println("query "+query);
			SolrMetadataClient solr = new SolrMetadataClient();
			SolrDocumentList docs = solr.search(query);
			if (docs != null)
				for (SolrDocument doc : docs) {
					l.add(toHtml(doc));
				}
		}
		return l;
	}

	static public void main(String[] args) {
		/*
		 * if (args.length > 0) { String query = computeSolrQuery(args);
		 * System.out.println("query "+query); SolrMetadataClient solr = new
		 * SolrMetadataClient(); SolrDocumentList docs = solr.search(query); if
		 * (docs != null) for (SolrDocument doc : docs) {
		 * System.out.println(doc); }
		 * 
		 * }
		 */
		List<String> l = run(args);

		for (String doc : l) {
			System.out.println(doc);
		}

	}
}
