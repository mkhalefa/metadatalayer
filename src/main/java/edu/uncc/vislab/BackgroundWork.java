package edu.uncc.vislab;

import java.util.Map;
import java.util.Set;

import edu.uncc.vislab.util.Annontate;
import edu.uncc.vislab.util.FSCrawler;
import edu.uncc.vislab.util.POSTMetadata;
import edu.uncc.vislab.util.Summarize;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BackgroundWork implements Runnable {

	private Log log = LogFactory.getLog(BackgroundWork.class);
	String path;
	String name;

	Map<String, String> params;

	BackgroundWork(String name, String path, Map<String, String> params) {
		this.name = name;
		this.path = path;

		this.params = params;
	}

	public void run() {
		// Client c=new Client();
		// c.send( " Crawler.run("+name+","+ path+") "+ " ---
		// "+"Annontate.run("+name+".xml"+","+ name+"o.xml);");
		//

		FSCrawler.run(name, path, params);
		Annontate.run(name+".xml", name+".i.xml", "./getmetadata.sh");
		Annontate.run(name+".i.xml", name+".j.xml", "./getcsvmetadata.sh");
		Summarize.run(name+".j.xml", name+".o.xml");
		POSTMetadata.run(name, name + ".o.xml");
	}
}