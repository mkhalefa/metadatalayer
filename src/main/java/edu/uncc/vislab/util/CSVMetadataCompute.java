package edu.uncc.vislab.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.StringWriter;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import org.apache.commons.cli.*;

public class CSVMetadataCompute {

	static String inputFilePath;
	static boolean extract_types = false;

	static void parse(String args[]) {

		Options options = new Options();

		Option input = new Option("i", "input", true, "input file path");
		input.setRequired(true);
		options.addOption(input);

		Option types = new Option("t", "types", false, "Extract the types from the second lines");
		types.setRequired(false);
		options.addOption(types);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
			inputFilePath = cmd.getOptionValue("input");
			if (cmd.hasOption("types"))
				extract_types = true;
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("CSV", options);

			System.exit(1);
			return;
		}
	}

	static public String convertTime(long time) {
		Date date = new Date(time);
		Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
		return format.format(date);
	}

	public static void main(String args[]) {
		parse(args);
        if(!inputFilePath.endsWith("csv")){
        
        	System.out.println("<?xml version=\"1.0\" encoding=\"utf-8\"?>");

        	return;
        }
        	
		try {
			File file = new File(inputFilePath);
			FileReader r = new FileReader(file);
			BufferedReader bf = new BufferedReader(r);
			String h1, h2;
			String[] headers;
			String[] types = null;
			h1 = "";
			h2 = "";
			h1 = bf.readLine();
			if (extract_types)
				h2 = bf.readLine();

			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

			// root elements
			Document doc = docBuilder.newDocument();
			Element csv = doc.createElement("csv");
			if (!h1.contains("xml") && h1.contains(",")) {
				headers = h1.split(",");

				if (extract_types)
					types = h2.split(",");

				for (int i = 0; i < headers.length; i++) {
					Element metadata = doc.createElement("metadata");

					metadata.setAttribute("columns", headers[i]);
					if (extract_types)
						metadata.setAttribute("type", types[i]);
					csv.appendChild(metadata);
				}
			}
			doc.appendChild(csv);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			transformer.setOutputProperty(OutputKeys.STANDALONE, "no");
			StreamResult result = new StreamResult(System.out);
			transformer.transform(source, result);

		} catch (Exception e) {
		}
	}
}
