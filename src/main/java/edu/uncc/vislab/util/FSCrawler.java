package edu.uncc.vislab.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.json.*;
import org.apache.commons.cli.*;

public class FSCrawler {

	static String outputFilePath;
	static String id;
	static String db;

	static void parse(String args[]) throws javax.xml.parsers.ParserConfigurationException {

		Options options = new Options();

		Option output = new Option("i", "input", true, "input file path");
		output.setRequired(true);
		options.addOption(output);

		Option idoption = new Option("d", "id", true, "id");
		idoption.setRequired(false);
		options.addOption(idoption);

		Option database = new Option("db", "database", true, "database ");
		database.setRequired(true);
		options.addOption(database);

		CommandLineParser parser = new DefaultParser();
		HelpFormatter formatter = new HelpFormatter();
		CommandLine cmd;

		try {
			cmd = parser.parse(options, args);
			db = cmd.getOptionValue("db");
			outputFilePath = cmd.getOptionValue("output");
			id = cmd.getOptionValue("id");
		} catch (ParseException e) {
			System.out.println(e.getMessage());
			formatter.printHelp("utility-name", options);

			System.exit(1);
			return;
		}
	}

	static FileWriter output;

	static String addParams(Map<String, String> params) {
		String metadata = "<metadata ";
		Iterator it = params.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();

			metadata += " " + pair.getKey().toString() + "=\"" + pair.getValue().toString() + "\"";
		}
		metadata += "/>";
		return metadata;
	}

	public static void run(String name, String path, Map<String, String> params) {
		try {

			String filexml = name + ".xml";
			output = new FileWriter(filexml);
			output.write("<Dataset name=\"" + name + "\" >\n");

			if (!params.containsKey("name")) {
				params.put("name", name);
			}
			if (!params.containsKey("id")) {
				if (id == null) {
					UUID dataset_id = UUID.randomUUID();
					id = dataset_id.toString();
				} else
					params.put("id", id);

				params.put("id", id);
			}

			output.write(addParams(params));

			if (!path.isEmpty())
				crawl(new File(path), "\t");
			output.write("</Dataset>\n");
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		if (args.length == 2) {
			String n = args[0];
			run(args[0], args[1], new HashMap<String, String>());
		} else
			System.err.println("usage is  <output xml>  <dir>");
	}

	public static void crawl(File node, String tabs) throws IOException {

		if (node.isDirectory()) {
			UUID id = UUID.randomUUID();
			output.write(tabs + "<Directory name=\"" + node.getAbsoluteFile() + "\" >" + "\n");

			String[] subNote = node.list();
			if (subNote != null) {
				for (String filename : subNote) {
					crawl(new File(node, filename), tabs + "\t");
				}
			}
			output.write(tabs + "</Directory>" + "\n");
		} else if (node.isFile()) {
			UUID id = UUID.randomUUID();
			output.write(tabs + "<File name=\"" + node.getAbsoluteFile() + "\"/> \n");
		}

	}
}
