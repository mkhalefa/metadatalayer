package edu.uncc.vislab.util;

import java.io.File;
import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import edu.uncc.vislab.Conf;

public class POSTMetadata {
	static String URL = "/metadata/add/";

	public static void run(String name, String xmlfile) {
		try {
			CloseableHttpClient httpClient = HttpClients.createDefault();
			CloseableHttpResponse response = null;
			String result = null;
			try {

				String url = Conf.getString("globalserver.server") + ":" + Conf.getInt("globalserver.port") + URL
						+ name;
				HttpPost post = new HttpPost(url);
				StringBody p1 = new StringBody("name", ContentType.TEXT_PLAIN);
				FileBody f1 = new FileBody(new File(xmlfile));

				MultipartEntityBuilder builder = MultipartEntityBuilder.create();
				builder.addPart("n", p1).addPart("file", f1);

				HttpEntity reqEntity = builder.build();
				post.setEntity(reqEntity);

				response = httpClient.execute(post);
				HttpEntity resEntity = response.getEntity();
				if (resEntity != null) {
					// result = ConvertStreamToString(resEntity.getContent(),
					// "UTF-8");
					String charset = "UTF-8";
					String content = EntityUtils.toString(response.getEntity(), charset);
					System.out.println(content);
				}
				EntityUtils.consume(resEntity);
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				response.close();
				httpClient.close();
			}
		} catch (ClientProtocolException cl) {
		} catch (IOException ee) {
		}
	}
}
