import requests, json
from time import sleep

requirements_file="b.json"	# The output file of data management layer. Currently, it contains the required remote process groups (rpgs) to be created
rpg_parent_id_non_sec="5d9349c2-015a-1000-3459-d52d72733250"
rpg_parent_id_sec=""
url_sec_endpoint="https://localhost:9443/nifi-api/"
url_non_sec_endpoint="http://ec2-52-32-117-160.us-west-2.compute.amazonaws.com:8011/nifi-api/"
url_access_token=url_sec_endpoint+"access/token"
#url_rpg=url_non_sec_endpoint+"process-groups/"+rpg_parent_id_non_sec+"/remote-process-groups"
url_clientid_sec=url_sec_endpoint+"flow/client-id"
url_clientid_non_sec=url_non_sec_endpoint+"flow/client-id"
login_param={"username":"nifi_admin","password":"123"} #TODO: There must be another way to encode the password
he_login={"Content-Type":"application/x-www-form-urlencoded","charset":"UTF-8"} #NOTE: header content is passed as a dictionary
token_header={}	# This header will be included in rest API headers in secure NIFI
sec=False	# If True, then we deal wih secure NIFI
rpg_json_path="./rpg_jpl_model_tmp_2.json" # Template JSON file for a Remote Process Group
rpg_conn_path="./rpg_conn_jpl_model_tmp.json"	# Template JSON file for link between NIFI processor and a created RPG. Note that the ID of the created RPG must be updated after loading this file
rpg_no=1	# Number of required RPG(s) and connections to be created
rpg_list=[]	# List of created RPG(s)
rpg_con=[]	# List of created connections to created RPG(s)
remote_in_indx=0 # Index of remote input port for each RPG. Currently, it is assumed that each RPG has only one input port 
remote_in_id_list=[] # List of remote input port ID in each RPG. It is assumed that each RPG has only one input port to facilitate extraction of remote input port automatically
clientid=""	# Client ID
thr=60		# Time To Build variable is just a delay between POST call for NIFI
start_flow_id="805236ec-0159-1000-4fc5-c7fa2c8c0e0b"	#Processor ID to start workflow

########## STEP 1: OBTAIN TOKEN IF SECURE NIFI AND INITIALIZE REQUIRED PARAMETERS FOR SECURE MODE################
if sec:
	token_resp=requests.post(url_access_token,data=login_param,verify=False,headers=he_login) # verify should be False to avoid SSL verification. Login parameters is passed as data, not as auth parameter of requests module
	#token_resp=requests.post(url_access_token,data=login_param,verify=False) # verify should be False to avoid SSL verification. Login parameters is passed as data, not as auth parameter of requests module
	token_header={"Authorization":"Bearer "+token_resp.text} # This header contains required token for specified user. It will be passed to secure NIFI through future rest APIs
	url_clientid=url_clientid_sec
	parent_group_id=rpg_parent_id_sec
	url_rpg=url_sec_endpoint+"process-groups/"+rpg_parent_id_sec+"/remote-process-groups"
	url_rpg_update=url_sec_endpoint+"remote-process-groups/"
	url_rpg_conn=url_sec_endpoint+"process-groups/"+rpg_parent_id_sec+"/connections"
	url_start_flow=url_sec_endpoint+"processors/"+start_flow_id
else:
	url_clientid=url_clientid_non_sec
	parent_group_id=rpg_parent_id_non_sec
	url_rpg=url_non_sec_endpoint+"process-groups/"+rpg_parent_id_non_sec+"/remote-process-groups"
	url_rpg_update=url_non_sec_endpoint+"remote-process-groups/"
	url_rpg_conn=url_non_sec_endpoint+"process-groups/"+rpg_parent_id_non_sec+"/connections"
	url_start_flow=url_non_sec_endpoint+"processors/"+start_flow_id

########### STEP 2: OBTAIN CLIENTID ###############
clientid=requests.get(url_clientid,headers=token_header).text

############# STEP 3: LOAD THE REQUIREMENTS FILE ###########
with open(requirements_file,"r") as f_req:
	req=json.load(f_req)

############# STEP 4: LOAD TEMPLATE JSON FILE(S) (e.g., REMOTE PROCESS GROUP, CONNECTION, .. etc) #############
with open(rpg_json_path,'r') as f_rpg:
	rpg_json=json.load(f_rpg)
	rpg_json['revision']['clientId']=clientid
	rpg_json['component']['parentGroupId']=parent_group_id

with open(rpg_conn_path,'r') as f_rpg_conn:
	rpg_conn_json=json.load(f_rpg_conn)
rpg_conn_json['revision']['clientId']=clientid

############ STEP 5: CREATE REQUIRED RPG(S) AND CONNECTION(S) ##############
############ UPDATE NIFI TEMPLATES ACCORDING TO REQUIREMENTS ##########
for req_i in req['rpg']:
	rpg_json['component']['targetUri']=req_i['ip']
	#rpg_json['component']['targetUris']=req_i['ip']
	#rpg_json['component']['name']=req_i['ip']
	if sec:
		rpg_json['component']['targetSecure']=True	# Default value in template file is false
	rpg_json['component']['transportProtocol']='HTTP'	#TODO: Check if this has to be modified to HTTPS in secure NIFI
	# To add required input ports for RPG, first, update temporary input port template, clear already input ports loaded from template RPG file, then add required input ports with updated values
	for in_port_i in req_i['in_port']:
		temp_conn={}
		temp_conn['id']=in_port_i['id']
		#temp_conn['name']=in_port_i['name']
		rpg_json['component']['contents']['inputPorts'].append(temp_conn)
	with open("test.json","w+") as ff:
		json.dump(rpg_json,ff)
	ttb=thr	# Set threshold time to create required remote process group
	while(ttb>0):
		rpg_cr=requests.post(url_rpg,json=rpg_json,headers=token_header)	# We do not have to specify "Content-Type: application/json" in the header because it is automatically deduced by the "json" parameter in the requests.post command		
		if rpg_cr.status_code/100==2:
			# successeful creation of RPG
			rpg=rpg_cr.json()
			break
		else:
			# Failure to create RPG. Sleep for "ttb" seconds, then try again
			sleep(ttb)
			ttb-=5

	# Now, we create the link(s) to the newly created RPG
	for in_port_i in req_i['in_port']:	
		rpg_conn_json['component']['destination']['id']=in_port_i['id']
		rpg_conn_json['component']['destination']['groupId']=rpg['id']
		ttb=thr
		with open("test_conn.json","w+") as ff:
			json.dump(rpg_conn_json,ff)
		while(ttb>0):
			rpg_conn=requests.post(url_rpg_conn,json=rpg_conn_json,headers=token_header)	# We do not have to specify "Content-Type: application/json" in the header because it is automatically deduced by the "json" parameter in the requests.post command
			if rpg_conn.status_code/100==2:
				# successeful creation of connection to RPG
				break
			else:
				# Failure to create connection. Sleep for "ttb" seconds, then try again
				sleep(ttb)
				ttb-=5
		remote_in_id_list.append(rpg_conn.json())
 
########### STEP 6: ADD INPUT PORTS TO RGP AND ENABLE TRANSMISSION ################
	in_port_json={}	# JSON body to update the input port inside the created remote process group
	in_port_json['remoteProcessGroupPort']={}
	in_port_json['remoteProcessGroupPort']['groupId']=rpg['id']
	for in_port_i in req_i['in_port']:
		if in_port_i["transmitting"]:
			# Just to make sure we use the correct 'revision' for RPG, retrive it again
			rpg=requests.get(url_rpg_update+rpg['id'],headers=token_header).json()
			in_port_json['revision']=rpg_json['revision']
			in_port_json['remoteProcessGroupPort']['id']=in_port_i['id']
			in_port_json['remoteProcessGroupPort']['transmitting']=True
			rpg_in_json=requests.put(url_rpg_update+rpg['id']+"/input-ports/"+in_port_i['id'],json=in_port_json,headers=token_header).json()
	rpg_update=requests.get(url_rpg_update+rpg['id'],headers=token_header).json()
	rpg_list.append(rpg_update)

########### STEP 7: START WORKFLOW ################
start_flow_json=requests.get(url_start_flow).json()
start_flow_json['component']['state']='RUNNING'	# Change state to RUNNING to start the flow
start_flow=requests.put(url_start_flow,json=start_flow_json,headers=token_header)
print("flow starting status: ",start_flow.status_code)
